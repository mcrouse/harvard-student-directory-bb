<?php

/* Harvard Physics
 * Jacob Barandes
 * (C) Harvard University 2016
 */

/*************
 * Functions *
 *************/

// Define general global functions that handle the annoying task of testing
// whether a variable $var is trivial or not, using pass-by-reference (note
// the & symbol) because copying an undefined variable gives an error; strict
// equality === is used for '' to avoid regarding 0 or false as trivial --
// otherwise use empty() instead, which is the same as is_trivial() except
// that it returns true for 0, false, and '0' (See the PHP type comparison
// tables at http://php.net/manual/en/types.comparisons.php)
function is_trivial(&$var) {
  if ((! isset($var)) || is_null($var) || $var === '' || $var === array()) return true;
  else return false;
}
function is_nontrivial(&$var) {
  return ! is_trivial($var);
}

// PHP does not permit immediately accessing the general values of an
// array returned from a function; first one needs to store the returned
// array as a new variable $array and then access the value as $array[key];
// this function allow for skipping the intermediate step
function get_array_value($key = null, $array = array()) {
  return $array[$key];
}

// This function parses a string of the form 'name=value;...' and returns a
// corresponding associative array array('name' => 'value', ...)
function get_name_value_array($name_value_string = null) {
  $name_value_array = array();
  if (is_string($name_value_string)) {
    foreach (explode(';', trim($name_value_string)) as $name_value_pair) {
      if (strpos($name_value_pair, '=') !== false) {
        list($name, $value) = explode('=', trim($name_value_pair), 2);
        $name_value_array[$name] = $value;
      }
    }
  }
  return $name_value_array;
}

// Due to a PHP/DOMDocument bug, certain DOMDocument methods don't correctly
// convert ampersands while converting all other html entities; this function
// handles converting the ampersands
function encode_ampersands($string) {
  return str_replace('&', '&amp;', $string);
}

// Rather annoyingly, there's no PHP function that converts just &amp;'s
// back to & for use in URLs; this function accomplishes the task
function decode_ampersands($string) {
  return str_replace('&amp;', '&', $string);
}

