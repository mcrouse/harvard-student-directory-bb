<?php

/* Harvard Physics
 * Jacob Barandes
 * (C) Harvard University 2016
 */

$image_dir = 'images/';

/* This script (to be run from the command line using the php-cgi command)
 * outputs an SVG photoboard using data from a CSV file; to output to a
 * file, use > output.svg at the command line, being sure to use -q to
 * suppress the extraneous header "Content-type ..." in the first two lines;
 * to convert to grayscale, select all objects and use the Inkscape filter
 * "Color->Desaturate"; use Save a Copy... to export to a PDF; be sure to
 * install the necessary font files and to dowload all necessary photo image
 * files into an appropriate subdirectory before opening in Inkscape and
 * converting to a PDF
 *
 * The script follows a very simple procedure:
 *
 * 1. Create the SVG document object
 * 2. Read the CSV file to obtain an array of data, consisting of one
 * element per person and containing keys pointing to various data values
 * (names, titles, etc.)
 * 3. Create the SVG document elements (images, text boxes) that will display
 * the information in the SVG document
 * 4. Repeat steps 2 and 3 if there are different sets of data that should
 * appear in the same SVG document
 * 5. Output the SVG document
 *
 */

// Load the master configuration settings
require_once 'functions.php';
require_once 'XMLDocument.php';

/***********************
 * Create SVG Document *
 ***********************/

// Create the XMLDocument object that will eventually be output
$svg_document = new XMLDocument();

// Create the <svg></svg> tags, inside of which all other tags will go
$dom_svg = $svg_document->createElement(
  'svg',
  null,
  array(
    'xmlns:svg'   => 'http://www.w3.org/2000/svg',
    'xmlns'       => 'http://www.w3.org/2000/svg',
    'xmlns:xlink' => 'http://www.w3.org/1999/xlink',
    'version'     => '1.1',
    'id'          => 'svg0',
    #'width'       => '3532.5', // 39.25in, with a printable area of 37.75in
    #'height'      => '3532.5'
    'width'       => '4687.5', // 39.25in, with a printable area of 37.75in
    //'height'      => '4387.5'  // 48.75in, with a printable area of 47.25in
    'height'      => '3000.'  // 48.75in, with a printable area of 47.25in
  )
);

/******************
 * Load Raw Data *
 ******************/

// For the case of grad students, get the data from a text file 'grads.csv';
// the file is a text file in which each row is a last name, first name, and
// photo filename of the form LASTNAME_FIRSTNAME_MIDDLENAME.jpg

// Define the array of rows to be returned
$rows = array();
// Load the raw data file in read-only mode, storing the file handle as
// $file_handle
$first = true;
$file_name = "final_grads.csv";
if ($file_handle = fopen($file_name, 'r')) {
    while(! feof($file_handle))
    {
        //print_r(fgetcsv($file_handle));
        $rows[] = fgetcsv($file_handle);
    }
}

fclose($file_handle);


// Sort the rows by last name
sort($rows);
$i = 0;

//print_r($rows);
//print( count($rows));
// Store the data into a two-dimensional array
$data_array = array();
foreach ($rows as $row_string) {
    //list($last_name,$first_name,$advisor, $office, $storage_filename, ) = $row_string;
    list($last_name,$first_name,$first_advisor,$last_advisor, $office, $storage_filename, ) = $row_string;
    $first_advisor = trim($first_advisor);
    $last_advisor = trim($last_advisor);
  $data_array[] = array(
    'display_name'     => "$first_name $last_name",
    'office'     => "$office",
    'advisor'     => "$first_advisor $last_advisor",
    'storage_filename' => $storage_filename
  );
  //print_r($data_array);
}

// Exit if the data array is empty
if ((! is_array($data_array)) || (count($data_array) == 0))
  exit;

/***********************
 * Create SVG Elements *
 ***********************/

// Create and append the photoboard elements
#$cols = 16;
$cols = 20;
$row_num = 1;
$x_origin = 67.50;
$y_origin = -240;
//$image_width = 210.87;
$image_width = 225.02235;
$image_height = 225.02235;
$caption_width = $image_width;
$caption_height = 96.704544;
$caption_vert_offset = 12;
foreach ($data_array as $key => $datum) {
  $id = $key;
  if (($key >= 0) && ($key % $cols) == 0) $row_num++;
  $x = $x_origin + ($image_width * ($key % $cols));
  $y = $y_origin + (($image_height + $caption_height) * $row_num);
  //~print_r("Key $key, Row $row_num\n");
  //~print_r("x = " . ($x_origin + ($image_width * ($key % $cols))) . ", y = " . ($y_origin + ($image_height * $row_num)) . "\n");
  $dom_image = $svg_document->createElement(
    'image',
    null,
    array(
      'id'         => "image$id",
      'xlink:href' => $image_dir . $datum['storage_filename'],
      //'xlink:href' => "testPerson.jpg",
      //'xlink:href' => "tmp.png",
      'x'          => $x,
      'y'          => $y,
      'width'      => $image_width,
      'height'     => $image_height
    )
  );
  $dom_svg->appendChild($dom_image);
  $dom_rect = $svg_document->createElement(
    'rect',
    null,
    array(
      'id'     => "rect$id",
      'style'  => 'fill:none;fill-opacity:1;stroke-width:2;stroke-miterlimit:4;stroke-dasharray:none;stroke:#000000',
      'x'      => $x,
      'y'      => $y,
      'width'  => $image_width,
      'height' => $image_height
    )
  );
  $dom_svg->appendChild($dom_rect);
  $dom_flow_root = $svg_document->createElement(
    'flowRoot',
    null,
    array(
      'id'        => "flowRoot$id",
      'xml:space' => 'preserve',
      'style'     => 'font-size:40px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville'
    )
  );
  $dom_flow_region = $svg_document->createElement(
    'flowRegion',
    null,
    array(
      'id' => "flowRegion$id"
    )
  );
  $dom_rect = $svg_document->createElement(
    'rect',
    null,
    array(
      'id'     => "rect{$id}a",
      'style'  => 'font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville',
      'x'      => $x,
      'y'      => $y + $image_height + $caption_vert_offset,
      'width'  => $caption_width,
      'height' => $caption_height
    )
  );
  $dom_flow_region->appendChild($dom_rect);
  $dom_flow_root->appendChild($dom_flow_region);
  $dom_flow_para = $svg_document->createElement(
    'flowPara',
    $datum['display_name'],
    array(
      'id'    => "flowPara{$id}a",
      'style' => 'font-size:16px;text-anchor:middle;text-align:center'
    )
  );

  $dom_flow_root->appendChild($dom_flow_para);
  $dom_flow_para = $svg_document->createElement(
      'flowPara',
      $datum['office'],
      array(
          'id'    => "flowPara{$id}a",
          'style' => 'font-size:16px;text-anchor:middle;text-align:center'
      )
  );

  $dom_flow_root->appendChild($dom_flow_para);
  $dom_flow_para = $svg_document->createElement(
      'flowPara',
      $datum['advisor'],
      array(
          'id'    => "flowPara{$id}a",
          'style' => 'font-size:16px;text-anchor:middle;text-align:center'
      )
  );

  $dom_flow_root->appendChild($dom_flow_para);


  $dom_svg->appendChild($dom_flow_root);
}

// Create and append a text box containing the necessary legal notice
$dom_flow_root = $svg_document->createElement(
  'flowRoot',
  null,
  array(
    'id'        => "flowRootNotice1",
    'xml:space' => 'preserve',
    'style'     => 'font-size:40px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville'
  )
);
$dom_flow_region = $svg_document->createElement(
  'flowRegion',
  null,
  array(
    'id' => "flowRegionNotice1"
  )
);
$dom_rect = $svg_document->createElement(
  'rect',
  null,
  array(
    'id'     => "rectNotice1a",
    'style'  => 'font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville',
    'x'      => 2700,
    'y'      => $y + $image_height + $caption_height + $caption_vert_offset + 20,
    'width'  => 736.70569,
    'height' => 121.89361
  )
);
$dom_flow_region->appendChild($dom_rect);
$dom_flow_root->appendChild($dom_flow_region);
$dom_flow_para = $svg_document->createElement(
  'flowPara',
  'Not all photos available. The compilation or redistribution of information from Harvard University directories is forbidden.',
  array(
    'id'    => "flowParaTitle1",
    'style' => 'font-size:32px;text-anchor:middle;text-align:justify'
  )
);
$dom_flow_root->appendChild($dom_flow_para);
$dom_svg->appendChild($dom_flow_root);


// Create and append a text box containing the necessary legal notice
$dom_flow_root = $svg_document->createElement(
  'flowRoot',
  null,
  array(
    'id'        => "flowRootNotice1",
    'xml:space' => 'preserve',
    'style'     => 'font-size:40px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville'
  )
);
$dom_flow_region = $svg_document->createElement(
  'flowRegion',
  null,
  array(
    'id' => "flowRegionNotice1"
  )
);
$dom_rect = $svg_document->createElement(
  'rect',
  null,
  array(
    'id'     => "rectNotice1a",
    'style'  => 'font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Libre Baskerville;-inkscape-font-specification:Libre Baskerville',
    'x'      => 2700,
    'y'      => $y + $image_height + $caption_height + $caption_vert_offset + 20,
    'width'  => 736.70569,
    'height' => 121.89361
  )
);
$dom_flow_region->appendChild($dom_rect);
$dom_flow_root->appendChild($dom_flow_region);
$dom_flow_para = $svg_document->createElement(
  'flowPara',
  'Not all photos available. The compilation or redistribution of information from Harvard University directories is forbidden.',
  array(
    'id'    => "flowParaTitle1",
    'style' => 'font-size:32px;text-anchor:middle;text-align:justify'
  )
);
$dom_flow_root->appendChild($dom_flow_para);
$dom_svg->appendChild($dom_flow_root);

// Create and append a text box containing the necessary legal notice
$dom_flow_root = $svg_document->createElement(
  'flowRoot',
  null,
  array(
    'id'        => "flowRootNotice1",
    'xml:space' => 'preserve',
    //'style'     => 'font-size:40px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Times New Roman;',
    'style'     => 'font-size:40px;font-style:semi-bold;font-variant:normal;font-weight:normal;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Adobe Caslon Pro;',
  )
);
$dom_flow_region = $svg_document->createElement(
  'flowRegion',
  null,
  array(
    'id' => "flowRegionNotice1"
  )
);
$dom_rect = $svg_document->createElement(
  'rect',
  null,
  array(
    'id'     => "rectNotice1a",
    //'style'  => 'font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Times New Roman;',
    'style'  => 'font-style:semi-bold;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Adobe Caslon Pro;line-height:100%',
    'x'      => 90,
    'y'      => 100.0,
    'width'  => 4687.5,
    'height' => 321.89361
  )
);
$dom_flow_region->appendChild($dom_rect);
$dom_flow_root->appendChild($dom_flow_region);
$dom_flow_para = $svg_document->createElement(
  'flowPara',
  //'Maxwell Dworkin Ph.D. Student Directory',
  'MAXWELL DWORKIN - PH.D. STUDENT DIRECTORY',
  array(
    'id'    => "flowParaTitle1",
    'style' => 'font-size:120px;text-anchor:middle;text-align:justify;padding-bottom:0px;margin-bottom:0px;line-height:100%'
  )
);

$dom_flow_root->appendChild($dom_flow_para);
$dom_flow_para = $svg_document->createElement(
    'flowPara',
    //'Maxwell Dworkin Ph.D. Student Directory',
    'Computer Science and Electrical Engineering',
    array(
        'id'    => "flowParaTitle1",
        'style' => 'font-size:80px;text-anchor:top;text-align:justify;padding-top:0px;margin-top:0px;line-height:100%'
    )
);
$dom_flow_root->appendChild($dom_flow_para);

$dom_image = $svg_document->createElement(
    'image',
    null,
    array(
        'id'         => "logo",
        'xlink:href' => "seas_logo.png",
        //'xlink:href' => "tmp.png",
        'x'          => 4100.0,
        'y'          => 75.0,
        'width'      => 960.0/2.,
        'height'     => 380.0/2. 
    )
);
$dom_svg->appendChild($dom_image);
$dom_svg->appendChild($dom_flow_root);



// Append the <svg></svg> tags to the SVG document root
$svg_document->dom_root->appendChild($dom_svg);

/***********************
 * Output SVG Document *
 ***********************/

// Output the SVG document
print $svg_document->getXML();

?>
