<?php

/* Harvard Physics Digital Record System (D-RECS)
 * Jacob Barandes
 * (C) Harvard University 2016
 */

/* This is the master configuration script, and should be require_once'd by
 * any PHP script loaded directly by URL (primarily, these PHP scripts are
 * those located inside <www_path>/)
 *
 */

// Constants are auto-global; they do not require a global pre-declaration in
// functions and methods that use them; note that the syntax define() must be
// used for constants that take non-literal values (such as string
// concatenations)

/* Administrative Settings
 * Maintenance Scheduling
 * Path Settings
 * Database Settings
 * File Upload Settings
 * Image Settings
 * Special Headers
 * Core Global Functions and Classes
 * Session, Status Message Buffer, Login Token, and Navigation Helper
 * Logging, Error-Reporting, and Exception-Handling Functions
 * Benchmarking and Timing
 *
 * /

/***************************
 * Administrative Settings *
 ***************************/

// Define a global constant to announce that this config.php file has been
// loaded
const CONFIG_LOADED = 1;

// Define a global constant administrative email address
const ADMIN_EMAIL_ADDRESS = 'dbhelp@physics.harvard.edu';

// Define a global constant help email address
const HELP_EMAIL_ADDRESS = 'dbhelp@physics.harvard.edu';

// Define a toggle to determine whether to store status debug messages in the
// global MessageBuffer object (itself defined later) (note that after
// enabling this option, the user needs to log out and then log back in so
// that the MessageBuffer object in the session variables will start recording
// debug messages)
const DEBUG_MESSAGE_TOGGLE = 0;

// Define a constant that determines whether to carry out actual writing to
// databases
const DB_WRITE_ENABLED = 1;

// Define a constant that determines whether to be recording user visits,
// assuming that DB_WRITE_ENABLED is switched on
const RECORD_WEBSITE_VISITS = 1;

// Enable access restrictions
const ACCESS_RESTRICTIONS_ENABLED = 1;

// Enable hiding of GET query string (warning: may mess up the web browser's
// back/forward navigation buttons if switched on)
const HIDE_GET_QUERY_STRING = 0;

// Define the time in seconds after which a user's login cookie should expire
// (integer)
const LOGIN_EXPIRATION_TIME = 1800;

/**************************
 * Maintenance Scheduling *
 **************************/

// Define a global constant maintenance toggle; when 1, all scripts will show
// a "Site currently under scheduled maintenance" message and be otherwise
// inoperable
const MAINTENANCE_TOGGLE = 0;

// This code block needs to go at the very beginning, before any other code
// has had a chance to execute
if (MAINTENANCE_TOGGLE == 1) {
?>
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html>
  <head>
    <title>System undergoing scheduled maintenance work</title>
  </head>
  <body>
    <h1>System undergoing scheduled maintenance work</h1>
    <p>The system is currently undergoing scheduled maintenance work. Please try your request again soon.</p>
    <hr>
    <address>For questions, please contact <a href="<?php print htmlentities(ADMIN_EMAIL_ADDRESS); ?>"><?php print htmlentities(ADMIN_EMAIL_ADDRESS); ?></a></address>
  </body>
</html>
<?php exit;
}

/*****************
 * Path Settings *
 *****************/

// Store the www path /path/to/www/ and the system path /path/to/system/
define('WWW_PATH',    '/srv/www/');
define('SYSTEM_PATH', '/home/devuser/system/');

// Store paths for configuration files and library files
define('CONFIG_PATH', SYSTEM_PATH . 'config/');
define('LIB_PATH',    SYSTEM_PATH . 'lib/');

// Set include/require to search all the various paths
set_include_path(
  get_include_path() . PATH_SEPARATOR .
  CONFIG_PATH . PATH_SEPARATOR .
  LIB_PATH
);

// Store paths for initialization files, uploaded files, JavaScript source
// files, lock files, and log files
define('INI_PATH',          SYSTEM_PATH . 'ini/');
define('DATABASE_INI_PATH', INI_PATH    . 'databases/');
define('FILES_PATH',        WWW_PATH    . 'files/');
define('JS_PATH',           WWW_PATH    . 'js/');
define('LOCK_PATH',         SYSTEM_PATH . 'lock/');
define('LOG_PATH',          SYSTEM_PATH . 'log/');
define('TOOLS_PATH',        WWW_PATH    . 'tools/');

// Store the base URL by subtracting the HTTP document root path from the base
// path, using | rather than / as the regex boundaries to avoid colliding with
// the forward slashes in the HTTP document root path
if (isset($_SERVER) && array_key_exists('DOCUMENT_ROOT', $_SERVER)) {
  define('WWW_URL', preg_replace('|^' . trim($_SERVER['DOCUMENT_ROOT']) . '|', '', WWW_PATH));

  // Store URLs for CSS files, uploaded files, image files, JavaScript source
  // files, CGI files, general scripts/binary files, and log files
  define('CSS_URL',   WWW_URL . 'css/');
  define('FILES_URL', WWW_URL . 'files/');
  define('IMG_URL',   WWW_URL . 'img/');
  define('JS_URL',    WWW_URL . 'js/');
  define('CGI_URL',   WWW_URL . 'cgi-bin/');
  define('BIN_URL',   WWW_URL . 'bin/');
  define('LOG_URL',   WWW_URL . 'log/');

  // Store the URL for the master gateway that redirects the user based on the
  // GET query string
  define('GATEWAY_URL', WWW_URL);
}

/*********************
 * Database Settings *
 *********************/

// Define the standard value-separator for field values in the databases
const DB_SEP = ',';

// Define the delimiter between shortnames in a database longname
// <database_name>DB_DELIM<table_name>DB_DELIM<field_name>
const DB_DELIM = '.';

/************************
 * File Upload Settings *
 ************************/

// These global settings may be superceded by local settings defined in
// particular classes

// Maximum allowed photo file size in bytes
const MAX_FILE_SIZE = 2097152;

// Allowed MIME types
$allowed_mime_types = array(
  'text/plain',
  'text/enriched',
  'text/txt',
  'text/richtext',
  'application/pdf',
  'application/x-pdf',
  'application/acrobat',
  'text/pdf',
  'text/x-pdf',
  'application/force-download', // Seems to be the MIME type of certain PDF files
  'application/postscript',
  'application/rtf',
  'application/x-dvi',
  'application/msword',
  'application/mswrite',
  'application/msexcel',
  'application/vnd.ms-excel',
  'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  'application/zip',
  'image/bmp',
  'image/png',
  'image/x-xbitmap',
  'image/x-xpixmap',
  'image/gif',
  'image/jpeg',
  'image/tiff',
  'image/xbm',
  'image/xpm'
);

/******************
 * Image Settings *
 ******************/

// These global settings may be superceded by local settings defined in
// particular classes

// Define the maximum width and height of uploaded photo image files; fixing
// the ratio of the supplied image file, the file will be resized so that
// neither bound is violated
const MAX_PHOTO_WIDTH  = 400;
const MAX_PHOTO_HEIGHT = 400;

/*******************
 * Special Headers *
 *******************/

// Use the existence of DOCUMENT_ROOT as a proxy for determining whether the
// calling script is part of the web frontend or not
if (isset($_SERVER) && array_key_exists('DOCUMENT_ROOT', $_SERVER)) {
  // Hide server details
  header('Server: ');
  header('X-Powered-By: ');

  // Prevent caching and force the user's browser client to refresh all
  // content in HTTP/1.0 and HTTP/1.1:
  header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
  header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
  header('Cache-Control: no-store, no-cache, must-revalidate');
  header('Cache-Control: post-check=0, pre-check=0', false);
  header('Pragma: no-cache');
}

/*************************************
 * Core Global Functions and Classes *
 *************************************/

// Define a function that takes a list of PHP class-definition filenames
// (without file extensions) and require_once's each of them
function load_libraries() { // Variable-length list of arguments
  $filenames = func_get_args();
  foreach ($filenames as $filename)
    require_once("$filename.php");
}

load_libraries(
  'classes/BASIC_CLASS',
  'functions/library_handling_functions',
  'functions/variable_checking_functions',
  'functions/string_functions',
  'functions/array_functions',
  'functions/comparison_functions',
  'functions/xhtml_functions',
  'functions/class_handling_functions'
);

/**********************************************************************
 * Session, Status Message Buffer, Login Token, and Navigation Helper *
 **********************************************************************/

// Use the existence of DOCUMENT_ROOT as a proxy for determining whether the
// calling script is part of the web frontend or not
if (isset($_SERVER) && array_key_exists('DOCUMENT_ROOT', $_SERVER)) {
  // Note that any objects that should be stored in session variables
  // (MessageBuffer objects being an example) must have their classes declared
  // before starting the session, or else one gets
  // "__PHP_Incomplete_Class Object" errors; the reason is that PHP
  // unserializes everything in the session storage; if a class isn't defined,
  // then PHP unserializes it into a __PHP_Incomplete_Class
  load_libraries(
    'classes/tools/MessageBuffer',
    'classes/tools/LoginToken',
    'classes/tools/UserPermissions',
    'classes/tools/NavigationHelper'
  );

  // This command is necessary for session data in the auto-global array
  // $_SESSION to be written to the server and remembered between page loads
  session_start();

  // Set a PHP configuration setting to ensure that session data survive at
  // least an hour
  ini_set('session.gc_maxlifetime', 3600);

  // The following static method call only creates a new message buffer if the
  // session doesn't already contain it; to empty the message buffer, perhaps
  // after displaying all the messages to the user, use the static method
  // MessageBuffer::clear_message_buffer(), and then the following call will
  // create a new one automatically the next time any script runs
  MessageBuffer::load_message_buffer_into_session();

  //~ MessageBuffer::clear_message_buffer();
}

/**************************************************************
 * Logging, Error-Reporting, and Exception-Handling Functions *
 **************************************************************/

load_libraries(
  'functions/logging_functions',
  'functions/error_reporting_functions',
  'functions/exception_handling_functions'
);

/***************************
 * Benchmarking and Timing *
 ***************************/

load_libraries('classes/tools/Timer');
