<?php

/* Harvard Physics
 * Jacob Barandes
 * (C) Harvard University 2016
 */

/* This script defines the XMLDocument class, an instance of which contains
 * various properties and methods appropriate to an XML document and any of
 * its subclasses; this class is not defined as an extension of DOMDocument,
 * because it is meant to be a higher-level script that hides the lower-level
 * calls to DOMDocument
 *
 * This class can be instantiated on its own in order to define arbitrary XML
 * documents
 *
 * USAGE:   <XMLDocument_Object> = new XMLDocument();
 *          XMLDocument::set_attributes(<DOMElement_Object>, <attributes>);
 *          XMLDocument::set_node_value(<DOMElement_Object>, <node_value>);
 *          <DOMElement_Object> = <XMLDocument_Object>->createElement(<node_name>[, <node_value>, <attributes>]);
 *          <DOMText_Object> = <XMLDocument_Object>->createTextNode(<node_value>);
 *          <DOMEntityReference_Object> = <XMLDocument_Object>->createEntityReference(<reference_value>);
 *          <string> = <XMLDocument_Object>->getXML();
 *
 * Note that putting text between a DOMElement's opening and closing tags
 *
 * <tag> ... </tag>
 *
 * can be done in several equivalent ways:
 *
 * (1) by specifying <node_value> immediately when using createElement()
 * (2) by assigning the nodeValue property of the DOMElement after the fact
 * (3) by creating a text node and then using appendChild to append the text
 *     node as a child element of the original DOMElement
 *
 * In light of options (1) and (2) above, one rarely needs to create and
 * append text nodes manually
 *
 */

// Define the XMLDocument class
class XMLDocument {

  // Declare class constants that should hold for all objects of the given
  // class; class constants must be scalars, are always public and static, and
  // can be redefined by subclasses, but cannot change in value at runtime

  // This constant string is a placeholder for empty tags <tag></tag>; its
  // value should be something unique, because it will later be replaced
  // wherever it appears in the final XML document with empty strings ''; note
  // also that it shouldn't contain any characters that would be replaced by
  // htmlentities() (string)
  const EMPTY_NODE_VALUE = '(!-- empty --)';

  // Declare static class variables that should hold for all objects of the
  // given class; to avoid unexpected behavior, static variables should not be
  // altered at runtime

  // Tags that must be closed <tag></tag> (as opposed to <tag />) even when
  // containing no content; can be redefined as needed in subclasses (array)
  protected static $tags_always_closed = array();

  // Declare properties and their default values (leaving out default values
  // means that they are null, and isset(...) returns false);
  // private values are visible only within this class definition;
  // protected values are visible to subclasses as well;
  // public values are visible everywhere

  // XML DOM document root (DOMElement Object)
  public $dom_root;

  // This public static method takes in a given DOMElement as a reference and
  // a list of attribute name-value pairs (either as an associative array or
  // as a string) and adds the attributes to the element accordingly; note
  // that htmlentities() is explicitly not used here, because PHP's
  // DOMDocument system calls it automatically when setting attributes
  final public static function set_attributes(DOMElement &$dom_element, $attributes = null) {

    // If $attributes is a string 'name=value;...', then first turn it into an
    // associative array of the form array('name' => 'value', ...)
    if (is_string($attributes))
      $attributes = get_name_value_array($attributes);

    if (is_array($attributes)) {
      foreach ($attributes as $attribute_name => $attribute_value)
        $dom_element->setAttribute((string)$attribute_name, utf8_encode((string)$attribute_value));
    }

  }

  // This public static method takes in a given DOMElement as a reference and
  // a new node value and changes the node value of the element accordingly;
  // note that htmlentities() is explicitly not used here, because PHP's
  // DOMDocument system calls it automatically
  // NOTE: Due to a PHP/DOMDocument bug, ampersands are not correctly escaped
  // in setting nodeValues, but are correctly escaped in setting attributes
  final public static function set_node_value(DOMElement &$dom_element, $node_value = null) {
    $dom_element->nodeValue = encode_ampersands(utf8_encode((string)$node_value));
  }

  // This special private method is the constructor for the class; it is
  // automatically called for every new instance of the class, and the special
  // $this variable refers reflexively to that new instance; the special
  // constant self refers to the entire class; arguments to this method are
  // passed when instantiating new objects of this class; can be redefined as
  // needed in subclasses
  function __construct() {
    $this->createDomRoot();
    $this->setDomDocumentOptions();
  }

  // This private method creates a new DOMDocument object as the property
  // $this->dom_root of the given XMLDocument object
  final private function createDomRoot() {
    $this->dom_root = new DOMDocument();
  }

  // This private method sets flags to ensure that the XML is properly
  // formatted and indented in the final output; can be redefined as needed
  // in subclasses
  protected function setDomDocumentOptions() {
    $this->dom_root->formatOutput = true;
    $this->dom_root->preserveWhiteSpace = false;
  }

  // Because the $this->dom_root property is not public (the present class is
  // meant to operate at a higher level), a method is needed to create new
  // elements; this public method returns the newly-created DOMElement object;
  // having a custom method is also useful for streamlining the process of
  // setting attributes, via an optional $attributes argument of the form
  // array('name' => 'value', ...) or a string 'name=value;...'; note that
  // htmlentities() is explicitly not used here, because PHP's DOMDocument
  // system calls it automatically
  // NOTE: Due to a PHP/DOMDocument bug, ampersands are not correctly escaped
  // in setting nodeValues, but are correctly escaped in setting attributes
  final public function createElement($node_name = null, $node_value = null, $attributes = null) {

    try {
      if (is_trivial($node_name)) throw new Exception('Missing or invalid XML DOM element node name.');

      // If the given $node_name is, in fact, an object rather than a string,
      // with its own create() method, then instead execute that object's own
      // create() method, with the present XMLDocument object as the argument;
      // that create method will generally call back the present createElement
      // method, but now with a proper string $node_name
      if (is_object($node_name) && method_exists($node_name, 'create')) {
        $dom_element = $node_name->create($this);
        return $dom_element;
      }

      // Determine if a node value has been specified as an argument, and, if
      // so, then include it in creating the element
      if (is_nontrivial($node_value))
        $dom_element = $this->dom_root->createElement(
          $node_name,
          encode_ampersands(utf8_encode((string)$node_value))
        );
      // The PHP DOMDocument class doesn't know that some tags must have the
      // form <tag></tag> even when the child node (i.e., the content between
      // the tags) is empty; by default, PHP writes <tag /> in that case, even
      // when it's erroneous; sending a '' as an argument when creating the
      // element is not enough -- one must explicitly define the childNode
      // property for these tags; the static::EMPTY_NODE_VALUE is given, and
      // then replaced later on, before outputting the final XML to the web
      // browser
      elseif (in_array($node_name, static::$tags_always_closed))
        $dom_element = $this->dom_root->createElement($node_name, static::EMPTY_NODE_VALUE);
      else
        $dom_element = $this->dom_root->createElement($node_name);

      // If an $attributes argument has been provided, then add the
      // appropriate attributes to the new element
      static::set_attributes($dom_element, $attributes);

      return $dom_element;
    }
    catch (Exception $e) {
      var_dump($e->getMessage());
      return false;
    }

  }

  // Because the $this->dom_root property is not public (the present class is
  // meant to operate at a higher level), a method is needed to create new
  // text nodes; this public method returns the newly-created DOMText object
  final public function createTextNode($node_value = null) {
    return $this->dom_root->createTextNode($node_value);
  }

  // Because the $this->dom_root property is not public (the present class is
  // meant to operate at a higher level), a method is needed to create new
  // XML entity references; this public method returns the newly-created
  // DOMEntityReference object
  final public function createEntityReference($reference_value = null) {
    return $this->dom_root->createEntityReference($reference_value);
  }

  // This public method puts together the various ingredients and returns the
  // full XML document as a string
  final public function getXML() {

    try {
      if ($xml_output = $this->dom_root->saveXML())
        $xml_output = str_replace(static::EMPTY_NODE_VALUE, '', $xml_output);
      else throw new Exception('Unable to output XML document');

      // Return the full XML-formatted document as a string (note that some
      // alternative approaches involve reprocessing through a sequence of
      // saveXML, loadXML, saveXML, but that can mess up various features; for
      // example, empty tags <tag></tag> erroneously become <tag/>); the
      // header <?xml version="1.0"> is prepended automatically
      return sprintf("%s", $xml_output);
    }
    catch(Exception $e) {
      var_dump($e->getMessage());
      return false;
    }

  }

}

?>
