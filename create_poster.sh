#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
cd "${DIR}"
echo `pwd`
echo "Building SVG from PHP"
php $DIR/svg_cs_photoboard.php > $DIR/final.svg
echo "Done building SVG"
echo "Converting to PDF"
inkscape --export-pdf=$DIR/final.pdf $DIR/final.svg
echo "Done - PDF file is: final.pdf"
