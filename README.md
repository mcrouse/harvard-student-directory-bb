# Tutorial for Student Directory

1. Update the Excel file (according to the format in template.csv ) with all the students you would like to be in the poster - the final must be saved as csv and the name "final_grads.csv"
2. Put all the corresponding images of the students in the images directory (currently images are roughly 300px x 300 px)
3. To create the pdf for printing, double click the file "create_poster.sh". This will popup a window and execute all the code for generating the poster which will be saved as "final.pdf"


